﻿using System;
using System.IO;

namespace Iodynis.Libraries.Asciiing.Example
{
    public static class Program
    {
        public static void Main(params string[] arguments)
        {
            Asciier asciier = new Asciier("../../font.txt");

            Console.WriteLine(asciier.Asciify("ASCII is awesome!"));
            Console.WriteLine(asciier.Asciify("0123456789"));
            Console.WriteLine(asciier.Asciify("ABCDEFGHIJKLM"));
            Console.WriteLine(asciier.Asciify("NOPQRSTUVWXYZ"));
            Console.WriteLine(asciier.Asciify("abcdefghijklm"));
            Console.WriteLine(asciier.Asciify("nopqrstuvwxyz"));
            Console.WriteLine(asciier.Asciify(" ?!.,:;\"()+-_="));

            File.WriteAllText("./output.txt", asciier.Asciify("0123456789\r\nABCDEFGHIJKLMNOPQRSTUVWXYZ\r\nabcdefghijklmnopqrstuvwxyz\r\n ?!.,:;\"()+-_=\r\n\r\nASCII-art is awesome!"));

            Console.ReadKey();
        }
    }
}
