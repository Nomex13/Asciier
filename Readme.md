# Asciier

A small library for ASCII text generation.

## Usage

The code

```csharp
Asciier asciier = new Asciier("font.txt");
Console.WriteLine(asciier.Asciify("ASCII-art is awesome!"));
```

will print

```
┌─┐ ┌─┐ ┌─┐ ┬ ┬           │       ┌                ┌             | 
├─┤ └─┐ │   │ │ ─── ┌┐ ┌─ ├     ° └┐     ┌┐ │││ ╒═ └┐ ┌┐ ┌┬┐ ╒═  | 
└ ┘ └─┘ └─┘ ┴ ┴     └┴ │  └     │  ┘     └┴ └┴┘ └─  ┘ └┘ │││ └─  . 
```

to the console. The font used is https://gitlab.com/iodynis/Asciier/-/blob/master/Examples/Simple/font.txt .

## License

Library is available under the MIT license.

Repository icon is from https://ikonate.com/ pack and is used under the MIT license.